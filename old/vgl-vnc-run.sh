#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

# Parse arguments
if [ "$#" -lt 2 ]
then
	die "Usage: ./vgl-vnc-run.sh <image_name> <container_name> <command_to_run> [<command_arg>+] [-- <docker-argument>+]"
fi

IMAGE_NAME=$1
CONTAINER_NAME=$2
DOCKER_ARGUMENTS=""
DOCKER_CMD=$3
for (( i=4; i <= "$#"; i++ )); do
	x=${!i}
	if [ "$x" = "--" ]; then
		j=i+1
		DOCKER_ARGUMENTS="${@:j}"
		break
    else
    	DOCKER_CMD="$DOCKER_CMD $x"
	fi
done

# Require privilege
if [[ $UID -ne 0 ]]; then
    die "Please run this script with root privileges"
fi

# Check NVIDIA drivers are installed and active
if [[ -z $(lsmod | grep nvidia) ]]
then
	die "NVIDIA drivers are not active, install them and reboot"
fi

# Start X server if necessary
if [ -z $(ls /tmp/.X11-unix/X0) ]
then
	echo "No X server found on display :0, starting one"
	sudo nvidia-xconfig -a --allow-empty-initial-configuration --use-display-device=None
	export DISPLAY=":0"
	sudo nohup Xorg $DISPLAY &
	xhost +SI:localuser:root # Allow local user to connect to X server
fi

# Ask for password for VNC server
echo -n "Enter a password for the VNC server: "
read -s VNC_PASSWD
echo ""

# Run container as a daemon process, this will run the vgl-vnc-entrypoint.sh script then the user-supplied command
export VNC_PORT=5901
docker run \
	-d --rm \
	--init \
	-e DISPLAY \
	--runtime=nvidia \
	-v /tmp/.X11-unix/:/tmp/.X11-unix/ \
	--name $CONTAINER_NAME \
	$DOCKER_ARGUMENTS \
	$IMAGE_NAME

if [ $? -eq 0 ]
then
	docker exec -it $CONTAINER_NAME $DOCKER_CMD
else
	die "Error running docker command"
fi
