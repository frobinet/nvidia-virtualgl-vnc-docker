#!/bin/bash

echo "
# We're using the /tmp/.X11-unix from the host, so we should always properly kill the VNC server before finishing
kill_vnc() {
	#/opt/TurboVNC/bin/vncserver -kill $DISPLAY;
	echo \"killed VNC server\"
}

trap_term() {
	echo \"Received TERM Signal\"
	kill -TERM \$child 2>/dev/null
	kill_vnc
}

# Docker stop will send SIGTERM (15)
trap trap_term TERM

# Actually run command (in background)
$@ &

child=\$!
wait \$child
trap - TERM
wait \$child

# Cleanup on termination in any case
kill_vnc
" > command.sh
exec bash command.sh