#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

# This is run within the container on startup (as the Dockerfile ENTRYPOINT)
# to start the VNC server using the password from variable VNC_PASSWD

# Export DISPLAY if it's not exposed yet
if [ -z $DISPLAY ]
then
	export DISPLAY=:1
fi

# Check that VNC_PASSWD is set
if [ -z $VNC_PASSWD ]
then
	die "Coudln't read VNC password from variable 'VNC_PASSWD'"
fi

# Start VNC server
mkdir -p ~/.vnc
echo $VNC_PASSWD | /opt/TurboVNC/bin/vncpasswd -f > ~/.vnc/passwd
chmod 0600 ~/.vnc/passwd

trap_term() {
	echo "Received TERM Signal"
	kill -TERM \$child 2>/dev/null
	/opt/TurboVNC/bin/vncserver -kill $DISPLAY;
	echo "Killed VNC server"
}

# Docker stop will send SIGTERM so we handle it by shutting down vnc server (and killing sleep process)
trap trap_term TERM

# vncserver returns immediately but we started the container so we wait forever
/opt/TurboVNC/bin/vncserver -3dwm -noxstartup $DISPLAY
sleep infinity &

child=$!
wait $child
trap - TERM
wait $child

# Execute command passed to "docker run" which will be received as argument to this script
# This is a bit convoluted, because we need to handle SIGTERM signals sent to the container upon "docker stop"
# To do so, we just exec a different "command.sh" file that executes the command argument given to this script,
# but wraps it in logic to properly shutdown the VNC server when the docker container gets shutdown. 

#echo "
# We're using the /tmp/.X11-unix from the host, so we should always properly kill the VNC server before finishing
#kill_vnc() {
#	/opt/TurboVNC/bin/vncserver -kill $DISPLAY;
#	echo \"Killed VNC server\"
#}

#trap_term() {
#	echo \"Received TERM Signal\"
#	kill -TERM \$child 2>/dev/null
#	kill_vnc
#}


#trap trap_term TERM

# Actually run command (in background)
#$@ & 

#
#wait \$child
#trap - TERM
#wait \$child

# Cleanup on termination in any case
#kill_vnc
#" > command.sh
#exec bash command.sh

