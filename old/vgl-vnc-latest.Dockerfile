# Start from NVIDIA image with OPENGL and CUDA support
FROM nvidia/cudagl:9.2-devel-ubuntu16.04

ARG VIRTUALGL_VERSION=2.6
ARG TURBOVNC_VERSION=2.2
ARG LIBJPEG_VERSION=2.0.0
ARG VNC_SCREEN=1
ARG VNC_PORT=5901

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display

# Install needed dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    libglu1-mesa-dev mesa-utils \
    wget curl\
    ca-certificates \
    x11-xkb-utils xterm xauth xfonts-base xkb-data libxv1 libxv1:i386

# Install and configure Virtual GL and TurboVNC
RUN cd /tmp && \
    curl -fsSL \
        -O https://svwh.dl.sourceforge.net/project/turbovnc/${TURBOVNC_VERSION}/turbovnc_${TURBOVNC_VERSION}_amd64.deb \
        -O https://svwh.dl.sourceforge.net/project/libjpeg-turbo/${LIBJPEG_VERSION}/libjpeg-turbo-official_${LIBJPEG_VERSION}_amd64.deb \
        -O https://svwh.dl.sourceforge.net/project/virtualgl/${VIRTUALGL_VERSION}/virtualgl_${VIRTUALGL_VERSION}_amd64.deb && \
    dpkg -i *.deb && \
    rm -f /tmp/*.deb && \
    /opt/VirtualGL/bin/vglserver_config -config +s +f -t && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*



WORKDIR /home/root
COPY vgl-vnc-entrypoint.sh /home/root/vgl-vnc-entrypoint.sh
RUN chmod +x vgl-vnc-entrypoint.sh

ENTRYPOINT ["/home/root/vgl-vnc-entrypoint.sh"]
CMD ["bash"]
