#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

if ! type /opt/TurboVNC/bin/vncviewer > /dev/null
then
	die "Couldn't find TurboVNC at /opt/TurboVNC/bin/vncviewer"
fi

if [ "$#" -ne 2 ]
then
	die "Usage: ./vgl-vnc-connect.sh <user> <host>"
fi

# Connect with a client through an SSH tunnel
if ![ -z VNC_PORT ]; then
	VNC_PORT=5901
fi
SSH_COMMAND="ssh -L ${VNC_PORT}:localhost:${VNC_PORT} -N -f -l $1 $2"
if [[ -z $(ps -e | grep ssh | grep "${VNC_PORT}:localhost:${VNC_PORT}") ]]
then
	echo "Attempting to connect to $2 as $1"
	echo $SSH_COMMAND
	eval $SSH_COMMAND
else
	echo "Reusing existing SSH tunnel"
fi

if [ $? -ne 0 ]
then
	die "Failed to establish SSH forwarding."
fi

/opt/TurboVNC/bin/vncviewer localhost:${VNC_PORT}
