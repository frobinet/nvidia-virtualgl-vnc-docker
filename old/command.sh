
# We're using the /tmp/.X11-unix from the host, so we should always properly kill the VNC server before finishing
kill_vnc() {
	#/opt/TurboVNC/bin/vncserver -kill /private/tmp/com.apple.launchd.vcuDVcNSJK/org.macosforge.xquartz:0;
	echo "killed VNC server"
}

trap_term() {
	kill -TERM $child 2>/dev/null
	touch proof
	kill_vnc
}

# Docker stop will send SIGTERM (15)
trap trap_term TERM

# Actually run command (in background)
sleep 100 &

child=$!
echo "Parent $$ Waiting for child $child"
wait $child
trap - TERM
wait $child

# Cleanup on termination in any case
echo "blah"
kill_vnc

