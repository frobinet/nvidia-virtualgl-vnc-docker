#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

# Parse arguments
if [ "$#" -ne 3 ]
then
	die "Usage: ./vgl-vnc-connect.sh <user> <host> <vnc_port>"
fi
USER=$1
HOST=$2
VNC_PORT=$3

# Check TurboVNC
TURBO_VNC=/opt/TurboVNC/bin/vncviewer
if ! type $TURBO_VNC > /dev/null
then
	die "Couldn't find TurboVNC at /opt/TurboVNC/bin/vncviewer"
fi


SSH_COMMAND="ssh -L ${VNC_PORT}:localhost:${VNC_PORT} -N -f -l $USER $HOST"
if [[ -z $(ps -e | grep ssh | grep "${VNC_PORT}:localhost:${VNC_PORT}") ]]
then
	echo "Attempting to connect to $2 as $1"
	echo $SSH_COMMAND
	eval $SSH_COMMAND
else
	echo "Reusing existing SSH tunnel"
fi

if [ $? -ne 0 ]
then
	die "Failed to establish SSH forwarding."
fi

/opt/TurboVNC/bin/vncviewer localhost:${VNC_PORT}
