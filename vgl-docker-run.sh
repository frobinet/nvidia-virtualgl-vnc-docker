#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

# Parse arguments
if [ "$#" -lt 2 ]
then
	die "Usage: ./vgl-docker-run.sh <image_name> <container_name> <command_to_run> [<command_arg>+] [-- <docker-argument>+]"
fi

IMAGE_NAME=$1
CONTAINER_NAME=$2
DOCKER_ARGUMENTS=""
DOCKER_CMD=$3
for (( i=4; i <= "$#"; i++ )); do
	x=${!i}
	if [ "$x" = "--" ]; then
		j=i+1
		DOCKER_ARGUMENTS="${@:j}"
		break
    else
    	DOCKER_CMD="$DOCKER_CMD $x"
	fi
done

# Use nvidia runtime (nvidia-docker2) if it's available
NVIDIA_RUNTIME=""
if [ -x "$(command -v nvidia-smi)" ]; then
	NVIDIA_RUNTIME="--runtime=nvidia"
fi

if [ -z DISPLAY ]; then
	die "No value for environment variable 'DISPLAY'. This variable should be set to the display you want VNC to run on (eg. DISPLAY=:1)."
fi

# Actually run the docker container
docker run \
	-e DISPLAY \
	-v /tmp/.X11-unix/:/tmp/.X11-unix/ \
	$NVIDIA_RUNTIME \
	--name $CONTAINER_NAME \
	$DOCKER_ARGUMENTS \
	$IMAGE_NAME $DOCKER_CMD

