
# Explain Principle

- why remote rendering on (potentially headless) server
- why does VNC + VGL solve the problem?
- Why we want a proper full desktop environment behind VNC (for windowing)


# Preliminaries

- Assumes docker is installed properly (can test this using `docker run hello-world`)

## SSH Setup for the client/server

- Include SSH server configuration (including public key and authorized_keys file)


## NVIDIA Drivers (optional)

- Drivers
- Nvidia docker 2 installation + test


# Host Setup

- Build docker image containing VirtualGL. Here, I will be building from an `ubuntu:16.04` base:
`docker build -t vgl:ubuntu-16.04 -f vgl-generic.Dockerfile . --build-arg VGL_ROOT_IMAGE=ubuntu:16.04`

For NVIDIA CUDA support, see the real example section with Autoware.


- Why we need X server running on display :0


# Client Setup

- Explain need for SSH tunnel
- Install TurboVNC client (or another one, probably works fine)


# A first accelerated Docker container

- Explain VGL Dockerfile
- Also mention that allowing access to Xorg server from container breaks encapsulation, but hey...
- Test with glxgears



# Example: running Autoware on a remote machine

- good example of heavy application that needs loads of RAM and GPU
- give dockerfile


