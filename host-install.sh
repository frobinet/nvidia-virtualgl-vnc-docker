#!/bin/bash

# This script installs the required dependencies that will be running on the *host machine*, no within the docker container.
# The following are installed: TurboVNC, Gnome desktop and libjpeg-turbo

VIRTUALGL_VERSION=2.6
TURBOVNC_VERSION=2.2
LIBJPEG_VERSION=2.0.0

# Require root for apt-get
if [[ $UID -ne 0 ]]; then
    die "Please run this script with root privileges"
fi

# Install needed dependencies
apt-get update && apt-get install -y --no-install-recommends gnome-core curl ca-certificates openssh-server

# Install and configure Virtual GL and TurboVNC
cd /tmp && \
curl -fsSL \
    -O https://svwh.dl.sourceforge.net/project/turbovnc/${TURBOVNC_VERSION}/turbovnc_${TURBOVNC_VERSION}_amd64.deb \
    -O https://svwh.dl.sourceforge.net/project/libjpeg-turbo/${LIBJPEG_VERSION}/libjpeg-turbo-official_${LIBJPEG_VERSION}_amd64.deb \
    -O https://svwh.dl.sourceforge.net/project/virtualgl/${VIRTUALGL_VERSION}/virtualgl_${VIRTUALGL_VERSION}_amd64.deb && \
dpkg -i *.deb && \
rm -f /tmp/*.deb && \
/opt/VirtualGL/bin/vglserver_config -config +s +f -t && \
apt-get clean && \
rm -rf /var/lib/apt/lists/*
