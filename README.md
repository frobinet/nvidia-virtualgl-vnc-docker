# Hardware Accelerated OpenGL Support for Docker Containers

Provides support for running Docker containers using hardware accelerated OpenGL applications and making them available to client computers over VNC.

This is useful if you have powerful desktop servers which you want to use for rendering heavy applications (scientific computing, games, ...).

This is targeted at servers running NVIDIA graphics cards, but could be adapted to others. The technologies in uses are VirtualGL, TurboVNC, and nvidia-docker2.


# Usage

## Server Machine

The server is assumed to have an NVIDIA card, with `docker`, the latest appropriate NVIDIA drivers and `nvidia-docker2` installed.

1. Install latest Nvidia drivers for your card.
You can check the current version using `nvidia-smi`.
Reboot after any installation and check that the NVIDIA driver has kicked in using `lsmod | grep nvidia`. The output should not be empty.

2. Build docker image.
```
sudo docker build -t vgl-vnc:latest -f vgl-vnc-latest.Dockerfile .
```

### Running the container

You should simply be able to use:
```
./nvidia-vgl-run.sh <your_command>
```

To run an OpenGL app with hardware acceleration, run it with `vglrun <command>` (from within the container). 

To test hardware acceleration, use 
```
./nvidia-vgl-run.sh vglrun /opt/VirtualGL/bin/glxspheres64 | grep "render"
```

The output should contain "direct rendering: yes" and your NVIDA card should be listed as the renderer.

To test your VNC setup, use:

```
./nvidia-vgl-run.sh vglrun /opt/VirtualGL/bin/glxspheres64
```

and connect your client (see dedicated section).

Note that to prevent loss of data, the containers are *not* automatically removed, so you need to use `sudo docker rm <container_name>` to remove them.


#--------------------

# Start VNC server within docker (on different display than the one used by Xorg on server)
export DISPLAY=:1
/opt/TurboVNC/bin/vncserver -3dwm $DISPLAY

# Run OpenGL application on VNC display
vglrun /opt/VitualGL/bin/glxspheres64

#--------------------

# Connect with a client through an SSH tunnel
export VNC_PORT=5901
ssh -L $VNC_PORT:localhost:$VNC_PORT -N -f -l fro tesla
/opt/TurboVNC/bin/vncviewer localhost:$VNC_PORT