#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

if [ -z $DISPLAY ]
then
	die "No value for environment variable 'DISPLAY'. This variable should be set to the display you want VNC to run on."
fi

# Start X server if necessary
if [ -z $(ls /tmp/.X11-unix/X0) ]
then
	echo "No X server found on display :0. Attempt to start one with root privileges."
	if [ -x nvidia-xconfig ]; then
		sudo nvidia-xconfig -a --allow-empty-initial-configuration --use-display-device=None
	fi
	sudo nohup Xorg :0 &
fi

# Allow local users to connect to X server (the root user from the docker container)
DISPLAY=:0 xhost +SI:localuser:root

# Start turbovnc 
export TVNC_WM=2d # Will use Gnome 2D
if ! [ -x "$(command -v gnome-session)" ]; then
	die "Couldn't locate GNOME, install the gnome-core package"
fi

TURBOVNC=/opt/TurboVNC/bin/vncserver
if [ -x $TURBOVNC ]
then
	$TURBOVNC -vgl $DISPLAY && echo "Successfully started VNC server on display $DISPLAY"
else
	die "Couldn't start TurboVNC from $TURBOVNC"
fi
