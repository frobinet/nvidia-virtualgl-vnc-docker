
ARG VGL_ROOT_IMAGE

FROM $VGL_ROOT_IMAGE

ENV DISPLAY=:1

# The following are only relevant for hosts with NVIDIA cards
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},display

# Install needed dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    libglu1-mesa-dev mesa-utils curl ca-certificates \
    x11-xkb-utils xterm xauth xfonts-base xkb-data libxv1

# Install and configure Virtual GL
ARG VIRTUALGL_VERSION=2.6
RUN cd /tmp && \
    curl -fsSL -O https://svwh.dl.sourceforge.net/project/virtualgl/${VIRTUALGL_VERSION}/virtualgl_${VIRTUALGL_VERSION}_amd64.deb && \
    dpkg -i *.deb && \
    rm -f /tmp/*.deb && \
    /opt/VirtualGL/bin/vglserver_config -config +s +f -t && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# By default, just run `glxgears` to test the setup
# This assumes that the device with the GPU is plugged to display :0
CMD ["vglrun", "-d :0.0", "glxgears"]
